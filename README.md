# BLE-R2D2
Do you have a bored R2D2 made by Disney like this?

![r2d2 disney boxed](docs/r2d2_box.jpg)

**Bring them to life with an Arduino BLE!**

Video: [R2D2 working on YouTube](https://youtu.be/PToDi8vSJeg)

## What do you need?
- Arduino with BLE connection. I have used a RedBearLab Blend V1.0.
- R2D2 made by Disney as in the picture. You can find it in all Disney's Stores.
- iPhone compatible (check the Arduino BLE compatibility table). I made the project only for iOS but you can try to do for Android or your favorite OS :)
- Electronic stuff as welder, cables, jumpers, etc.
- 1x [Phototransistor TIL111, to allow the "action functionality"](docs/pictures/til111.jpg). (You can use other if you want and if you know how it works)
- 1x 390Ω resistor, or similar.
- 6x fresh batteries

## Steps


### - Are you sure?
`Following this project disables the main operating modes 1 and 2.`

`Please, read all the process more than one time to be sure that you understand everything and you know how to do everything without problems.`

`Please, stop here if you are not sure with your welding skills.`

`I don't want any muerdered R2D2 :_(`

`If you want, you can change the Arduino connectors in the Arduino project's code.`

`The project has more pictures to have a look before start.`


### - [WHEELS] Connect wires from main board to Arduino board
Have a look of the picture.
 
![main board labeled](docs/pictures/pcb_up1_connecting.jpg)

- Connect the point M1A (main board) to the digital connector #0 (Arduino board)
- Connect the point M1B (main board) to the digital connector #1 (Arduino board)
- Connect the point M2A (main board) to the digital connector #3 (Arduino board)
- Connect the point M2B (main board) to the digital connector #4 (Arduino board)



### - [ACTION] Connect wires from main board to Arduino board
![TIL111 schema](docs/pictures/til111.jpg)

- Connect the point B1 (main board) to the connector #4 (TIL111)
- Connect the point B2 (main board) to the connector #5 (TIL111)
- Connect the connector 1 (TIL111) to the digital connector #6 (Arduino board), adding a resistor between both connectors
- Connect the connector 2 (TIL111) to the connector GND (Arduino board)



### - Cut 4 conductive tracks on the main board
Have a look of the picture and find the painted tracks in red.

![main board wires to cut](docs/pictures/pcb_up2_cut.jpg)

`Be sure and accurate with this process!`

Cut the 4 tracks with something sharp.



### - Power Arduino

To avoid a specific battery for the Arduino, you can connect the Arduino to the 6v circuit batteries.

![main board wires to cut](docs/pictures/pilas.jpg)


[![main board wires to cut](docs/pictures/board_connected_s.JPG)](docs/pictures/board_connected.JPG)

`I recomend to put 2 jumpers, outside the R2D2, to close the Arduino's batteries circuit.`

`Likewise, you can use some other technique to close the circuit.`


## - How it works?
Have a look of the pictures.

![main board wires to cut](docs/pictures/turn_off.JPG)
![main board wires to cut](docs/pictures/turn_on.JPG)

Left picture: TURNED OFF
- Original switch in off position.
- Jumper / (something to close the Arduino's power connector) open.
 

Right picture: TURNED ON
- Original switch in "Try me" position.
- Jumper / (something to close the Arduino's power connector) closed.
 
## Something missing or wrong?
`Please send me a message if you find any error.`

