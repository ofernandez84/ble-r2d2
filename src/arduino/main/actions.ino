
void action(int actionCode) {

  switch (actionCode) {
    case NO_ACTION:
      lastAction = actionCode;
      stopR2D2();
      break;
    case ACTION_HEAD:
      lastAction = actionCode;
      sayHiR2D2();
      break;
    case ACTION_RUN:
      lastAction = actionCode;
      runR2D2();
      break;
    case ACTION_BACK:
      lastAction = actionCode;
      backR2D2();
      break;
    case ACTION_LEFT:
      lastAction = actionCode;
      rotateLeftR2D2();
      break;
    case ACTION_RIGHT:
      lastAction = actionCode;
      rotateRightR2D2();
      break;
    case ACTION_RUN_LEFT:
      lastAction = actionCode;
      turnLeftR2D2();
      break;
    case ACTION_RUN_RIGHT:
      lastAction = actionCode;
      turnRightR2D2();
      break;
    case ACTION_BACK_LEFT:
      lastAction = actionCode;
      backLeftR2D2();
      break;
    case ACTION_BACK_RIGHT:
      lastAction = actionCode;
      backRightR2D2();
      break;
    default:
      lastAction = actionCode;
      stopR2D2();
      break;
  }
}

void sayHiR2D2() {
  Serial.println("SAY HI");
  
  digitalWrite(pin_HeadButton, HIGH);
  delay(actionButtonDelay);
  digitalWrite(pin_HeadButton, LOW);
}

void runR2D2() {
  Serial.println("RUN");
  
  digitalWrite(pin_LLegUp, HIGH);
  digitalWrite(pin_RLegUp, HIGH);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, LOW);
}

void backR2D2() {
  Serial.println("BACK");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, HIGH);
  digitalWrite(pin_RLegDown, HIGH);
}

void rotateLeftR2D2() {
  Serial.println("ROTATE LEFT");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, HIGH);
  digitalWrite(pin_LLegDown, HIGH);
  digitalWrite(pin_RLegDown, LOW);
}

void rotateRightR2D2() {
  Serial.println("ROTATE RIGHT");
  
  digitalWrite(pin_LLegUp, HIGH);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, HIGH);
}

void turnLeftR2D2() {
  Serial.println("TURN LEFT");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, HIGH);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, LOW);
}

void turnRightR2D2() {
  Serial.println("TURN RIGHT");
  
  digitalWrite(pin_LLegUp, HIGH);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, LOW);
}

void backLeftR2D2() {
  Serial.println("BACK LEFT");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, HIGH);
}

void backRightR2D2() {
  Serial.println("BACK RIGHT");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, HIGH);
  digitalWrite(pin_RLegDown, LOW);
}

void stopR2D2() {
  Serial.println("STOP");
  
  digitalWrite(pin_LLegUp, LOW);
  digitalWrite(pin_RLegUp, LOW);
  digitalWrite(pin_LLegDown, LOW);
  digitalWrite(pin_RLegDown, LOW);
}

