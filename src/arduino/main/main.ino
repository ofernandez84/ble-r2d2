#include <SPI.h>
#include <EEPROM.h>
#include <boards.h>
#include <RBL_nRF8001.h>
 
#define DIGITAL_OUT_PIN    LED_BUILTIN

char* deviceName = "ble_r2d2";
const String app_version = "0.1.0";

const int actionButtonDelay = 50;

// DON'T USE PINS 9 OR 8
const int pin_LLegUp = 0;
const int pin_LLegDown = 1;
const int pin_RLegUp = 3;
const int pin_RLegDown = 4;
const int pin_HeadButton = 6;

// ACTIONS
const int NO_ACTION = 0;
const int ACTION_HEAD = 2;
const int ACTION_RUN = 3;
const int ACTION_BACK = 5;
const int ACTION_LEFT = 7;
const int ACTION_RIGHT = 11;
const int ACTION_RUN_LEFT = 13;
const int ACTION_RUN_RIGHT = 17;
const int ACTION_BACK_LEFT = 19;
const int ACTION_BACK_RIGHT = 23;

int lastAction = 0;

void setup()
{
  Serial.println("Starting core version: " + app_version);
  
  // Default pins set to 9 and 8 for REQN and RDYN
  // Set your REQN and RDYN here before ble_begin() if you need
  //ble_set_pins(3, 2);
  
  // Set your BLE Shield name here, max. length 10
  ble_set_name(deviceName);
  
  // Init. and start BLE library.
  ble_begin();
  
  // Enable serial debug
  Serial.begin(57600);
  
  pinMode(DIGITAL_OUT_PIN, OUTPUT);
  pinMode(pin_LLegUp, OUTPUT);
  pinMode(pin_LLegDown, OUTPUT);
  pinMode(pin_RLegUp, OUTPUT);
  pinMode(pin_RLegDown, OUTPUT);
  pinMode(pin_HeadButton, OUTPUT);
}

void loop()
{
  static byte old_state = LOW;
  
  // If data is ready
  while(ble_available())
  {
    Serial.println("READING");
    digitalWrite(DIGITAL_OUT_PIN, HIGH);
    // read data
    byte data0 = ble_read();
    //Serial.println(data0);

    int newAction = data0;

    if (lastAction == newAction && newAction != NO_ACTION && newAction != ACTION_HEAD) {
      Serial.println("ACTION REPEATED");
    } else {
      action(newAction);
    }
  }
  
  if (!ble_connected())
  {
    Serial.println("NOT CONNECTED, " + app_version);
    digitalWrite(DIGITAL_OUT_PIN, LOW);
    
    delay(1000);
  }
  
  // Allow BLE Shield to send/receive data
  ble_do_events();  
}

