//
//  BLEManager.swift
//  BLE_test
//
//  Created by Oscar Fernández on 18/05/2017.
//  Copyright © 2017 Oscar Fernández. All rights reserved.
//
// following https://medium.com/@ryanjjones10/how-to-set-up-ble-with-swift-2-2-34bb6f209de2

import Foundation
import CoreBluetooth

class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    static var INSTANCE : BLEManager = BLEManager()
    
    private var centralManager: CBCentralManager!
    private var peripheral: CBPeripheral?
    private var deviceName: String?
    private var characteristics: [String : CBCharacteristic]?
    
    private static let DEVICE_STANDARD_NAME : String = "ble_r2d2" //Blend
    private static let CONNECT_OPTIONS : [String : Any] = [CBConnectPeripheralOptionNotifyOnDisconnectionKey : true]
    private static let BLEServiceChangedStatusNotification = "kBLEServiceChangedStatusNotificationR2D2"
    
    private static let RBL_SERVICE_UUID = "713D0000-503E-4C75-BA94-3148F18D941E"
    private static let RBL_CHAR_TX_UUID = "713D0002-503E-4C75-BA94-3148F18D941E"
    private static let RBL_CHAR_RX_UUID = "713D0003-503E-4C75-BA94-3148F18D941E"
    
    private override init() {
        super.init()
        self.characteristics = [String : CBCharacteristic]()
    }
    
    func start(deviceName : String = BLEManager.DEVICE_STANDARD_NAME) {
        self.deviceName = deviceName
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func stopAndReset() {
        self.centralManager.stopScan()
        
        if self.peripheral != nil {
            self.centralManager.cancelPeripheralConnection(self.peripheral!)
            
            self.peripheral?.delegate = nil
            self.peripheral = nil
            self.characteristics?.removeAll()
            
            sendBTServiceNotificationWithIsBluetoothConnected(false)
        }
    }
    
    func write(data: Data) {
        guard let char = self.characteristics?[BLEManager.RBL_CHAR_RX_UUID] else { return }
        
        self.peripheral?.writeValue(data, for: char, type: .withoutResponse)
    }
    
    func isConnected() -> Bool {
        return self.peripheral?.state == .connected
    }
    
    
    
    // CBCentralManagerDelegate //
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            self.centralManager?.scanForPeripherals(withServices: [CBUUID(string: BLEManager.RBL_SERVICE_UUID)], options: nil)
//            self.centralManager?.scanForPeripherals(withServices: [], options: nil)
            print("SCANNING")
        } else {
            print("ERROR - bluetooth powered off")
        }
    }
    
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        
    }
    
    // when scan and find some peripheral
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("device finded \(String(describing: peripheral.name))")
        if peripheral.name != nil && peripheral.name?.trimmingCharacters(in: .whitespacesAndNewlines) == self.deviceName {
            self.peripheral = peripheral
            self.centralManager.connect(self.peripheral!, options: BLEManager.CONNECT_OPTIONS)
        }
    }

    // when connect some peripheral
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if self.peripheral == peripheral {
            peripheral.delegate = self
            peripheral.discoverServices(nil)
            self.centralManager.stopScan()
            
            print("DEVICE CONNECTED - \(peripheral)")
            sendBTServiceNotificationWithIsBluetoothConnected(true)
        }
    }

    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        if self.peripheral == peripheral {
            print("FAIL TO CONNECT TO \(peripheral) : \(String(describing: error))")
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if self.peripheral == peripheral {
            if self.peripheral != nil {
                self.peripheral?.delegate = nil
                self.peripheral = nil
                self.characteristics?.removeAll()
                sendBTServiceNotificationWithIsBluetoothConnected(false)
            }
            //
            //        print("DEVICE DISCONNECTED", error ?? "no_error")
            //        self.start()
        }
    }
    
    
    
    // CBPeripheralDelegate //
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if self.peripheral == peripheral {
            peripheral.discoverCharacteristics(nil, for: peripheral.services![0])
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if self.peripheral == peripheral {
            for characteristic in service.characteristics! {
                let thisCharacteristic = characteristic as CBCharacteristic
                self.characteristics?[thisCharacteristic.uuid.uuidString] = thisCharacteristic
            }
        }
    }
    
    
    
    // Notifications //
    
    func startNotifications(observer: Any,selector: Selector) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(rawValue: BLEManager.BLEServiceChangedStatusNotification), object: nil)
    }
    
    func stopNotifications(observer: Any) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(BLEManager.BLEServiceChangedStatusNotification), object: nil)
    }
    
    private func sendBTServiceNotificationWithIsBluetoothConnected(_ isBluetoothConnected: Bool) {
        let connectionDetails = ["isConnected": isBluetoothConnected]
        NotificationCenter.default.post(name: Notification.Name(rawValue: BLEManager.BLEServiceChangedStatusNotification), object: self, userInfo: connectionDetails)
    }

}
