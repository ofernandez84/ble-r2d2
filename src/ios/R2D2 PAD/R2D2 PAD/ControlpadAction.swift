//
//  PadAction.swift
//  BLE_test
//
//  Created by Oscar Fernández on 19/05/2017.
//  Copyright © 2017 Oscar Fernández. All rights reserved.
//

import Foundation

enum ControlpadAction: UInt8 {
    case NoACTION = 0
    case HEAD = 2
    case UP = 3
    case DOWN = 5
    case LEFT = 7
    case RIGHT = 11
    case TURN_LEFT = 13
    case TURN_RIGHT = 17
    case BACK_LEFT = 19
    case BACK_RIGHT = 23
}

