//
//  PadActionExtViewController.swift
//  BLE_test
//
//  Created by Oscar Fernández on 19/05/2017.
//  Copyright © 2017 Oscar Fernández. All rights reserved.
//

import UIKit

extension ViewController {

    // View must be a square
    func getControlpadAction(from point: CGPoint, to view: UIView) -> ControlpadAction{
        
        let viewSize: CGFloat = view.layer.frame.width
        let bringToZero: CGFloat = viewSize / 2
        let centeredPoint: CGPoint = CGPoint(x: point.x - bringToZero, y: (point.y - bringToZero) * -1)
        let blackPoint: CGFloat = viewSize / 4 // 25%
        
        var returnValue : ControlpadAction = ControlpadAction.NoACTION
        
        // AT CENTER
        if abs(centeredPoint.y) < blackPoint && abs(centeredPoint.x) < blackPoint {
            returnValue = ControlpadAction.NoACTION
            
        // Y ZERO
        } else if centeredPoint.y == 0 {
            if centeredPoint.x > 0 {
                returnValue = ControlpadAction.RIGHT
            } else {
                returnValue = ControlpadAction.LEFT
            }
            
        } else {
            let xyRelation : CGFloat = centeredPoint.x / centeredPoint.y
            
            if xyRelation >= 2 { // B2F1
                
                if centeredPoint.x > 0 {
                    returnValue = ControlpadAction.RIGHT
                } else {
                    returnValue = ControlpadAction.LEFT
                }
                
            } else if xyRelation < 2 && xyRelation >= 0.5 { // GC
                
                if centeredPoint.y > 0 {
                    returnValue = ControlpadAction.TURN_RIGHT
                } else {
                    returnValue = ControlpadAction.BACK_LEFT
                }
                
            } else if xyRelation < 0.5 && xyRelation >= -0.5 { // HD
                
                if centeredPoint.y >= 0 {
                    returnValue = ControlpadAction.UP
                } else {
                    returnValue = ControlpadAction.DOWN
                }
                
            } else if xyRelation < -0.5 && xyRelation >= -2 { // AE
                
                if centeredPoint.y > 0 {
                    returnValue = ControlpadAction.TURN_LEFT
                } else {
                    returnValue = ControlpadAction.BACK_RIGHT
                }
                
            } else { // xyRelation < -2 // B1F2
                
                if centeredPoint.x >= 0 {
                    returnValue = ControlpadAction.RIGHT
                } else {
                    returnValue = ControlpadAction.LEFT
                }
            }
        }
        
        return returnValue
    }


}
