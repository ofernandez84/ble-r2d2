//
//  ViewController.swift
//  BLE_test
//
//  Created by Oscar Fernández on 18/05/2017.
//  Copyright © 2017 Oscar Fernández. All rights reserved.
//

import UIKit
import AudioToolbox

class ViewController: UIViewController {

    @IBOutlet weak var mainButton: UIButton!
    @IBAction func mainButtonAction(_ sender: Any) {
        bleManager.isConnected() ? disconnect() : connect()
    }
    @IBOutlet weak var actionButton: UIButton!
    @IBAction func actionButtonAction(_ sender: Any) {
        headAction()
    }
    @IBOutlet weak var padView: UIView!
    let padLayer = CAShapeLayer()
    
    var feedbackGenerator : UINotificationFeedbackGenerator?
    
    var bleManager : BLEManager = BLEManager.INSTANCE
    var timeLastControllerpadAction : Double = Date().timeIntervalSince1970
    let secMinIntervalBetweenControllerpadActions : Double = 0.2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.feedbackGenerator = UINotificationFeedbackGenerator()
        setButtonsAs(connected: bleManager.isConnected())
        
        // Watch Bluetooth connection
        bleManager.startNotifications(observer: self, selector: #selector(ViewController.connectionChanged(_:)))
        
        addJoystickStyle()
    }

    deinit {
        bleManager.stopNotifications(observer: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        //print("touchesBegan: \(String(describing: touches.first?.location(in: self.padView)))")
//    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touchedPoint : CGPoint = touches.first?.location(in: self.padView) {
            
            // padView is a square
            let padSize : CGFloat = self.padView.layer.frame.height
            
            if touchedPoint.x > 0 && touchedPoint.y > 0
                && touchedPoint.x < padSize && touchedPoint.y < padSize {
                
                // INSIDE SQUARE
                
                // move padLayer
                self.movePadTo(point: touchedPoint)
                
                // min interval
                let currentTime : Double = Date().timeIntervalSince1970
                
                if timeLastControllerpadAction + secMinIntervalBetweenControllerpadActions < currentTime {
                    timeLastControllerpadAction = currentTime
                    
                    //print("touchesMoved: \(String(describing: touches.first?.location(in: self.padView)))")
                    let padAction = getControlpadAction(from: touchedPoint, to: self.padView)
                    
                    print("PAD: \(padAction)")
                    
                    bleManager.write(data: Data(bytes: [padAction.rawValue]))
                }
                
            } else {
                // outside pad
                movePadToOrigin()
                hapticFeedback()
            }
            
        }
    
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("touchesEnded: \(String(describing: touches.first?.location(in: self.padView)))")
        movePadToOrigin()
        print("PAD: \(ControlpadAction.NoACTION)")
        bleManager.write(data: Data(bytes: [ControlpadAction.NoACTION.rawValue]))
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        movePadToOrigin()
//        print("touchesCancelled: \(String(describing: touches.first?.location(in: self.padView)))")
        print("PAD: \(ControlpadAction.NoACTION)")
        bleManager.write(data: Data(bytes: [ControlpadAction.NoACTION.rawValue]))
    }
    
    // buttons actions
    func connect() {
        mainButton.isEnabled = false
        bleManager.start()
    }
    
    func disconnect() {
        mainButton.isEnabled = false
        actionButton.isEnabled = false
        bleManager.stopAndReset()
    }

    func headAction() {
        bleManager.write(data: Data(bytes: [ControlpadAction.HEAD.rawValue]))
    }
    
    func setButtonsAs(connected: Bool) {
        if connected {
            mainButton.setTitle("Disconnect", for: .normal)
            actionButton.isEnabled = true
        } else {
            mainButton.setTitle("Connect", for: .normal)
            actionButton.isEnabled = false
        }
        
        mainButton.isEnabled = true
    }
    
    
    // Notification action
    func connectionChanged(_ notification: Notification) {
        // TO-DO
        let userInfo = (notification as NSNotification).userInfo as! [String: Bool]
        //print("notifications said \(String(describing: userInfo["isConnected"]))")
        
        if userInfo["isConnected"] != nil && userInfo["isConnected"]! {
            print("isConnected notification: connected")
            setButtonsAs(connected: true)
            
        } else {
            print("isConnected notification: not connected")
            setButtonsAs(connected: false)
        }
    }
    
    
    // Joystick
    func addJoystickStyle() {
        let x = padView.layer.position.x
//        let xZ = padView.layer.frame.origin.x
        let y = padView.layer.position.y
//        let yZ = padView.layer.frame.origin.y
        let width = padView.layer.frame.width
        let height = padView.layer.frame.height
        
        // PAD
        let radius : CGFloat = width / 6
        let arcCenter : CGPoint = CGPoint(x: x , y: y )
        
        let circlePath = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: CGFloat(0), endAngle: CGFloat(Double.pi * 2), clockwise: true)
        self.padLayer.path = circlePath.cgPath
        self.padLayer.fillColor = nil
        self.padLayer.strokeColor = UIColor.black.cgColor
        self.padLayer.lineWidth = 3.0
        
        self.view.layer.addSublayer(self.padLayer)
        
        
        
        // BORDER
        let squareRect = CGRect(x: 0, y: 0, width: width, height: height)
        let squarePath = UIBezierPath(roundedRect: squareRect, cornerRadius: 10)
        let borderLayer : CAShapeLayer = CAShapeLayer()
        borderLayer.path = squarePath.cgPath
        borderLayer.fillColor = nil
        borderLayer.strokeColor = UIColor.black.cgColor
        borderLayer.lineWidth = 3.0
//        borderLayer.lineJoin = kCALineCapRound
//        borderLayer.lineDashPattern = [6,10]
        
        self.padView.layer.backgroundColor = nil
        self.padView.layer.addSublayer(borderLayer)
        
    }
    
    func movePadTo(point : CGPoint) {
        CATransaction.begin()
        //CATransaction.setAnimationDuration(0.0)
        CATransaction.setValue(true, forKey: kCATransactionDisableActions)
        
        self.padLayer.frame.origin = CGPoint(x: point.x - self.padView.layer.frame.height / 2, y: point.y - self.padView.layer.frame.width / 2)
        //self.padLayer.position = CGPoint(x: touchedPoint.x - self.padView.layer.frame.height / 2, y: touchedPoint.y - self.padView.layer.frame.width / 2)
        
        CATransaction.commit()
    }
    
    func movePadToOrigin() {
        self.padLayer.frame.origin = CGPoint(x: 0, y: 0)
    }

    func hapticFeedback() {
//        UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
        
        let version = UIDevice.current.value(forKey: "_feedbackSupportLevel") as! Int
        
        if version == 1 {
            
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            
        } else if version == 2 {
            // not tested
            self.feedbackGenerator?.notificationOccurred(.warning)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
